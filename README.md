## springboot打造基于RBAC的权限管理系统
## QQ讨论群：588579508
## 简要设计
[简要设计说明https://my.oschina.net/xiaozhutefannao/blog/1600612](https://my.oschina.net/xiaozhutefannao/blog/1600612)
## 演示地址
[演示地址](http://47.94.4.215:8085/oda) 账号admin 密码1 如有访问出错的情况，请刷新浏览器缓存后再试。
## 部署
1. 导入项目
2. 刷脚本，springboot.sql
3. 运行项目即可

## 系统图
### 菜单管理
![菜单管理](http://wx2.sinaimg.cn/mw690/006qiLqogy1fm9e4feqe7j311s0ifgo9.jpg)
### 用户管理
![用户管理](http://wx3.sinaimg.cn/mw690/006qiLqogy1fm9e3cqebfj311x0iimzk.jpg)
### 系统日志
![系统日志](http://wx2.sinaimg.cn/mw690/006qiLqogy1fm9e475szzj311v0ictce.jpg)

## 待续（部分功能待完善）