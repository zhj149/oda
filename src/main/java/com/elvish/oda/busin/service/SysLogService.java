package com.elvish.oda.busin.service;

import org.nutz.dao.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.dao.LogDao;
import com.elvish.oda.busin.pojo.SysLog;
import com.elvish.oda.common.pojo.Page;

@Component
public class SysLogService {

	@Autowired
	private LogDao logDao;

	public QueryResult findAll(SysLog log, Page page) {
		return logDao.findAll(log, page);
	}

}
