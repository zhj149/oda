package com.elvish.oda.busin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.elvish.oda.busin.dao.MenuDao;
import com.elvish.oda.busin.pojo.Menu;

@Component
public class MenuService {

	@Autowired
	private MenuDao menuDao;

	public List<Menu> findAll() {
		return menuDao.findAll();
	}

	public Menu findById(Integer id) {
		return menuDao.findById(id);
	}

	public int save(Menu menu) {
		return menuDao.save(menu);
	}

	@Transactional
	public int deleteById(Integer id) {
		// 删除它所有的子菜单
		menuDao.deleteByPid(id);
		// 删除它自己
		return menuDao.deleteById(Menu.class, id);
	}

	public int update(Menu menu) {
		return menuDao.update(menu);
	}

}
