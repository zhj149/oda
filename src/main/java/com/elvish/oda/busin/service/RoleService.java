package com.elvish.oda.busin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.elvish.oda.busin.dao.RoleDao;
import com.elvish.oda.busin.pojo.Role;

@Component
public class RoleService {

	@Autowired
	private RoleDao roleDao;

	public List<Role> findAllRoles() {
		return roleDao.findAll();
	}

	public Role findById(Integer id) {
		return roleDao.findById(id);
	}

	public int save(Role role) {
		return roleDao.save(role);
	}

	public int update(Role role) {
		return roleDao.update(role);
	}

	public int deleteById(Integer id) {
		return roleDao.deleteById(id);
	}

	public int deleteByIds(String[] ids) {
		return roleDao.deleteByIds(ids);
	}

	@Transactional
	public int assignUserRole(Integer roleId, Integer userId) {
		// 删除已有角色
		// 这么操作也是为了多角色考虑
		// 缺陷 ： 1.用中间表设计方式在表格显示方面的缺陷
		roleDao.deleteRoleByUserId(userId);
		// 插入新角色
		roleDao.saveUserRole(userId, roleId);
		return 1;
	}

}
