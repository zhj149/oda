package com.elvish.oda.busin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.dao.UserDao;
import com.elvish.oda.busin.pojo.Permission;
import com.elvish.oda.busin.pojo.User;
import com.elvish.oda.common.pojo.Page;
import com.elvish.oda.common.utils.CommonUtil;

@Component
public class UserService {

	@Autowired
	private UserDao userdao;

	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 *            用户名
	 * @return 用户
	 */
	public User findByUsername(String username) {
		return userdao.findByUsername(username);
	}

	/**
	 * 查找当前登录用户的角色和权限
	 * 
	 * @return 角色和权限集合
	 */
	public List<Permission> findMyPermitions() {
		return userdao.findMyPermitions(CommonUtil.getCurrentUser());
	}

	/**
	 * 根据条件更新用户
	 * 
	 * @param user
	 *            用户
	 * @param pattern
	 *            条件正则表达式
	 * @return 1代表更新成功
	 */
	public int updateUserSelective(User user, String pattern) {
		return userdao.updateUserSelective(user, pattern);
	}

	/**
	 * 查找所有用户
	 * 
	 * @return 用户集合
	 */
	public List<User> findAll() {
		return userdao.findAll();
	}

	/**
	 * 
	 * @return
	 */
	public List<User> findAllUserRole(Page page) {
		return userdao.findAllUserRole(page);
	}

	public Integer count() {
		return userdao.count();
	}

	public int update(User user) {
		return userdao.updateUserSelective(user, "^delflag$");
	}

}
