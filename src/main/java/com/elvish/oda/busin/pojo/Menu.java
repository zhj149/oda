package com.elvish.oda.busin.pojo;

import java.util.List;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.Json;

@Table("t_menu")
public class Menu {
	@Id
	private Integer id;
	@Column("menu_name")
	private String menuName;
	@Column
	private String url;
	@Column
	private Integer pid;
	@Column("permission_code")
	private String permissionCode;
	@Column
	private String icon;
	@Column
	private Integer sortnum;

	private List<Menu> children;
	private String text;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public String getPermissionCode() {
		return permissionCode;
	}

	public void setPermissonCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<Menu> getChildren() {
		return children;
	}

	public void setChildren(List<Menu> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return Json.toJson(this);
	}

	public Integer getSortnum() {
		return sortnum;
	}

	public void setSortnum(Integer sortnum) {
		this.sortnum = sortnum;
	}

	public String getText() {
		return menuName;
	}

	public void setText(String text) {
		this.text = text;
	}

}
