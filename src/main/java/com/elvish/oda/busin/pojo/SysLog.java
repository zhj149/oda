package com.elvish.oda.busin.pojo;

import java.util.Date;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.Json;

import com.elvish.oda.busin.anno.Operator;

@Table("t_sys_log")
public class SysLog {
	@Column
	private Long id;
	@Column
	@Operator("like")
	private String username;
	@Column
	private Date date;
	@Column
	@Operator("like")
	private String url;
	@Column
	@Operator("like")
	private String method;
	@Column
	private String ip;
	@Column("class_method")
	private String classMethod;
	@Column
	private Object args;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getClassMethod() {
		return classMethod;
	}

	public void setClassMethod(String classMethod) {
		this.classMethod = classMethod;
	}

	public Object getArgs() {
		return args;
	}

	public void setArgs(Object args) {
		this.args = args;
	}

	@Override
	public String toString() {
		return Json.toJson(this);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
