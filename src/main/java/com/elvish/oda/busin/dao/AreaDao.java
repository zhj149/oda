package com.elvish.oda.busin.dao;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Criteria;
import org.nutz.dao.sql.Sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.pojo.Area;
import com.elvish.oda.common.utils.NutzSqlUtil;

@Component
public class AreaDao {

	@Autowired
	Dao dao;

	public List<Area> findAll() {
		return NutzSqlUtil.getEntities(dao, Area.class);
	}

	public Area findById(Integer id) {
		return NutzSqlUtil.findById(dao, Area.class, id);
	}

	public List<Area> findByPid(Integer id) {
		String delSql = "select * from t_area where pid = @pid";
		Sql sql = Sqls.fetchString(delSql);
		sql.setParam("pid", id);
		return NutzSqlUtil.getEntities(sql, dao, Area.class);
	}

	public int deleteByIds(String[] ids) {
		Criteria cri = Cnd.cri();
		cri.where().andIn("id", ids);
		dao.clear(Area.class, cri);
		return 1;
	}

	public <T> int save(T obj) {
		return NutzSqlUtil.save(dao, obj);
	}

	public <T> int deleteById(Class<T> clz, Integer id) {
		return NutzSqlUtil.deleteById(dao, clz, id);
	}

	public int deleteByPid(Integer id) {
		String delSql = "delete from t_area where pid = @pid";
		Sql sql = Sqls.fetchString(delSql);
		sql.setParam("pid", id);
		dao.execute(sql);
		return 1;
	}

	public <T> int update(T obj) {
		return dao.update(obj);
	}

}
