package com.elvish.oda.busin.dao;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Criteria;
import org.nutz.dao.sql.Sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.pojo.Menu;
import com.elvish.oda.common.utils.NutzSqlUtil;

@Component
public class MenuDao {

	@Autowired
	Dao dao;

	public List<Menu> findAll() {
		Criteria cri = Cnd.cri();
		cri.getOrderBy().asc("sortnum");
		return NutzSqlUtil.getEntities(dao, cri, Menu.class);
	}

	public Menu findById(Integer id) {
		return NutzSqlUtil.findById(dao, Menu.class, id);
	}

	public <T> int save(T obj) {
		return NutzSqlUtil.save(dao, obj);
	}

	public <T> int deleteById(Class<T> clz, Integer id) {
		return NutzSqlUtil.deleteById(dao, clz, id);
	}

	public int deleteByPid(Integer id) {
		String delSql = "delete from t_menu where pid = @pid";
		Sql sql = Sqls.fetchString(delSql);
		sql.setParam("pid", id);
		dao.execute(sql);
		return 1;
	}

	public <T> int update(T obj) {
		return dao.update(obj);
	}

}
