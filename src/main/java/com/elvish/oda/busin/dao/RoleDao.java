package com.elvish.oda.busin.dao;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Criteria;
import org.nutz.dao.sql.Sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.pojo.Role;
import com.elvish.oda.common.utils.NutzSqlUtil;

@Component
public class RoleDao {

	@Autowired
	private Dao dao;

	public List<Role> findAll() {
		return NutzSqlUtil.getEntities(dao, Role.class);
	}

	public Role findById(Integer id) {
		return dao.fetch(Role.class, id);
	}

	public int save(Role role) {
		dao.insert(role);
		return 1;
	}

	public int update(Role role) {
		dao.update(role);
		return 1;
	}

	public int deleteById(Integer id) {
		dao.delete(Role.class, id);
		return 1;
	}

	public int deleteByIds(String[] ids) {
		Criteria cri = Cnd.cri();
		cri.where().andIn("id", ids);
		dao.clear(Role.class, cri);
		return 1;
	}

	public int deleteRoleByUserId(Integer userId) {
		String delSql = "delete from t_user_role where user_id = @userid";
		Sql sql = Sqls.fetchString(delSql);
		sql.setParam("userid", userId);
		dao.execute(sql);
		return 1;
	}

	public int saveUserRole(Integer userId, Integer roleId) {
		String iSql = "insert into t_user_role values(@userid,@roleid)";
		Sql sql = Sqls.fetchString(iSql);
		sql.setParam("userid", userId);
		sql.setParam("roleid", roleId);
		dao.execute(sql);
		return 1;
	}

}
