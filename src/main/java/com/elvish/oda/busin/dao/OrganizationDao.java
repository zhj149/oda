package com.elvish.oda.busin.dao;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.pojo.Organization;
import com.elvish.oda.busin.pojo.SysLog;
import com.elvish.oda.common.pojo.Page;
import com.elvish.oda.common.utils.NutzSqlUtil;

@Component
public class OrganizationDao {

	@Autowired
	Dao dao;

	public int save(SysLog log) {
		return NutzSqlUtil.save(dao, log);
	}

	public QueryResult findAll(Page page, String[] ids) {
		Pager pager = dao.createPager(page.getPage(), page.getRows());
		Criteria cri = Cnd.cri();
		if (ids.length > 0) {
			cri.where().andIn("areaId", ids);
		}
		cri.getOrderBy().asc("id");
		List<Organization> list = dao.query(Organization.class, cri, pager);
		pager.setRecordCount(dao.count(Organization.class, cri));
		return new QueryResult(list, pager);
	}

	public <T> int save(T obj) {
		return NutzSqlUtil.save(dao, obj);
	}

	public <T> int update(T obj) {
		return dao.update(obj);
	}

	public Organization findById(Integer id) {
		return NutzSqlUtil.findById(dao, Organization.class, id);
	}

	public int deleteByIds(String[] ids) {
		Criteria cri = Cnd.cri();
		cri.where().andIn("id", ids);
		dao.clear(Organization.class, cri);
		return 1;
	}

}
