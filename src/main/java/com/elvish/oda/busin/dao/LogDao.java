package com.elvish.oda.busin.dao;

import java.util.List;

import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elvish.oda.busin.pojo.SysLog;
import com.elvish.oda.common.pojo.Page;
import com.elvish.oda.common.utils.CommonUtil;
import com.elvish.oda.common.utils.NutzSqlUtil;

@Component
public class LogDao {

	@Autowired
	Dao dao;

	public int save(SysLog log) {
		return NutzSqlUtil.save(dao, log);
	}

	public QueryResult findAll(SysLog log, Page page) {
		Pager pager = dao.createPager(page.getPage(), page.getRows());
		Criteria cri = CommonUtil.getCriteria(log);
		// 这个方法很方便，就是操作符需要自己重新写方法覆盖
		// Criteria cri = Cnd.from(dao, log);
		if (cri != null) {
			// 动态排序也可以，这里就不写了。
			cri.getOrderBy().desc("date");
		}
		List<SysLog> logs = dao.query(SysLog.class, cri, pager);
		pager.setRecordCount(dao.count(SysLog.class, cri));
		return new QueryResult(logs, pager);
	}

}
