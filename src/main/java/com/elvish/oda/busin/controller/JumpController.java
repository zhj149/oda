package com.elvish.oda.busin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.elvish.oda.busin.pojo.Area;
import com.elvish.oda.busin.pojo.Menu;
import com.elvish.oda.busin.pojo.Organization;
import com.elvish.oda.busin.pojo.Permission;
import com.elvish.oda.busin.pojo.Role;
import com.elvish.oda.busin.service.AreaService;
import com.elvish.oda.busin.service.MenuService;
import com.elvish.oda.busin.service.OrganizationService;
import com.elvish.oda.busin.service.PermissionService;
import com.elvish.oda.busin.service.RoleService;

/**
 * 页面跳转控制器
 * 
 * @author zhulongxiang
 *
 */
@Controller
public class JumpController {

	@Autowired
	private AreaService areaService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private OrganizationService organizationService;

	/**
	 * 区域管理
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/areaManage")
	public String goAreaManage() {
		return "/sys/areaManage";
	}

	/**
	 * 组织机构管理
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/organizationManage")
	public String goOrganizationManage() {
		return "/sys/organizationManage";
	}

	/**
	 * 菜单管理
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/menuManage")
	public String goMenuManage() {
		return "/sys/menuManage";
	}

	/**
	 * 菜单管理(这里只支持2级菜单，层级太深体验度太差)
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/menuManage/goOperMenu/{oper}/{level}/{id}")
	public String goOperMenu(Model model, @PathVariable("oper") String oper, @PathVariable("level") Integer level,
			@PathVariable("id") Integer id) {
		Menu menu = null;
		// 二级菜单新增
		if ("add".equalsIgnoreCase(oper) && level == 1) {
			menu = new Menu();
			menu.setPid(id);
		}
		// 二级菜单修改
		else if ("update".equalsIgnoreCase(oper) && level == 1) {
			menu = menuService.findById(id);
		}
		model.addAttribute("menu", menu);
		return "/sys/operMenu";
	}

	/**
	 * 用户管理
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/userManage")
	public String goUserManage() {
		return "/sys/userManage";
	}

	/**
	 * 拓扑测试
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/tuopu")
	public String goTuopu() {
		return "/sys/tuopu";
	}

	/**
	 * 拓扑测试
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/roleManage")
	public String goRoleManage() {
		return "/sys/roleManage";
	}

	/**
	 * 角色维护
	 * 
	 * @param model
	 * @param oper
	 *            操作符
	 * @param id
	 *            角色ID
	 * @return
	 */
	@GetMapping(value = "/sys/roleManage/goOperRole")
	public String goOperRole(Model model, String oper, Integer id) {
		Role role = null;
		// 更新
		if ("2".equals(oper)) {
			role = roleService.findById(id);
			model.addAttribute("role", role);
		}
		return "/sys/operRole";
	}

	/**
	 * 角色选择
	 * 
	 * @param userId
	 *            用户id
	 * @return
	 */
	@GetMapping(value = "/sys/roleManage/chooseRole")
	public ModelAndView goOperRole(String userId) {

		List<Role> roles = roleService.findAllRoles();

		ModelAndView view = new ModelAndView("/sys/chooseRole");
		view.addObject("userId", userId);
		view.addObject("roles", roles);
		return view;
	}

	@GetMapping(value = "/sys/permissionManage")
	public String goPermissionManage() {
		return "/sys/permissionManage";
	}

	/**
	 * 菜单管理(这里只支持2级菜单，层级太深体验度太差)
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/permissionManage/goOperPermission/{oper}/{level}/{id}")
	public String goOperPermission(Model model, @PathVariable("oper") String oper, @PathVariable("level") Integer level,
			@PathVariable("id") Integer id) {
		Permission permission = null;
		// 一级以下新增
		if ("add".equalsIgnoreCase(oper) && level == 1) {
			permission = new Permission();
			permission.setPid(id);
		}
		// 一级以下修改
		else if ("update".equalsIgnoreCase(oper) && level == 1) {
			permission = permissionService.findById(id);
		}
		model.addAttribute("permission", permission);
		return "/sys/operPermission";
	}

	/**
	 * 分配权限选择页面
	 * 
	 * @param model
	 * @param roleId
	 *            角色id
	 * @return
	 */
	@GetMapping(value = "/sys/roleManage/assignPermissions")
	public String goAssignPermissions(Model model, Integer roleId) {
		List<Permission> perms = permissionService.findByRoleId(roleId);
		model.addAttribute("perms", perms);
		model.addAttribute("roleId", roleId);
		return "/sys/assignPermissions";
	}

	@GetMapping(value = "/sys/syslog")
	public String goSyslog() {
		return "/sys/syslog";
	}

	/**
	 * 地域维护
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/areaManage/goOperArea/{oper}/{level}/{id}")
	public String goOperArea(Model model, @PathVariable("oper") String oper, @PathVariable("level") Integer level,
			@PathVariable("id") Integer id) {
		Area area = null;
		if ("add".equalsIgnoreCase(oper) && level == 1) {
			area = new Area();
			area.setPid(id);
		} else if ("update".equalsIgnoreCase(oper) && level == 1) {
			area = areaService.findById(id);
		}
		model.addAttribute("area", area);
		return "/sys/area_add_or_update";
	}

	/**
	 * 机构维护
	 * 
	 * @return
	 */
	@GetMapping(value = "/sys/organizationManage/goOperOrganization/{oper}/{id}")
	public String goOperOrganization(Model model, @PathVariable("oper") String oper, @PathVariable("id") Integer id) {
		Organization organization = null;
		if ("update".equalsIgnoreCase(oper)) {
			organization = organizationService.findById(id);
			model.addAttribute("organization", organization);
		}
		return "/sys/organization_add_or_update";
	}

}
