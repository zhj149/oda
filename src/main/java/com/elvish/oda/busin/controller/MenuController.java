package com.elvish.oda.busin.controller;

import java.util.List;

import org.nutz.lang.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elvish.oda.busin.pojo.Menu;
import com.elvish.oda.busin.service.MenuService;
import com.elvish.oda.common.pojo.Result;
import com.elvish.oda.common.utils.CommonUtil;
import com.elvish.oda.common.utils.ResultUtil;

@RestController
public class MenuController {

	@Autowired
	private MenuService menuService;

	/**
	 * 初始化菜单列表
	 * 
	 * @return 菜单tree集合
	 */
	@PostMapping(value = "/menu/initMenuTreeTable")
	public List<Menu> initMenuTreeTable() {
		List<Menu> menus = menuService.findAll();
		if (menus != null && menus.size() > 0) {
			List<Menu> treeList = CommonUtil.buildTree(menus, "pid");
			return treeList;
		}
		return null;
	}

	/**
	 * CRUD
	 * 
	 * @param menu
	 */
	@PostMapping(value = "/menu/operMenu/{oper}")
	public Result<Menu> operMenu(Menu menu, @PathVariable("oper") Integer operFlag) {
		Result result = new Result<>();
		try {

			if (operFlag == 1) {
				// 新增
				if (Lang.isEmpty(menu.getId())) {
					menuService.save(menu);
				}
				// 更新
				else {
					menuService.update(menu);
				}
			}
			// 删除
			else if (operFlag == 2) {
				menuService.deleteById(menu.getId());
			}
			result = ResultUtil.success();
		} catch (Exception e) {
			result = ResultUtil.fail(-1, "系统异常");
		}
		return result;
	}

}
