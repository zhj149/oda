package com.elvish.oda.busin.controller;

import java.util.List;

import org.nutz.lang.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elvish.oda.busin.pojo.Menu;
import com.elvish.oda.busin.pojo.User;
import com.elvish.oda.busin.service.UserService;
import com.elvish.oda.common.pojo.EasyUITableData;
import com.elvish.oda.common.pojo.Page;
import com.elvish.oda.common.pojo.Result;
import com.elvish.oda.common.utils.ResultUtil;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	/**
	 * 初始化用户表格
	 * 
	 * @param page
	 *            分页信息
	 * @return
	 */
	@PostMapping(value = "/user/initUserTable")
	public EasyUITableData initUserTable(Page page) {
		EasyUITableData data = new EasyUITableData();
		List<User> users = userService.findAllUserRole(page);
		Integer total = userService.count();
		if (users != null && users.size() != 0) {
			data.setRows(users);
			data.setTotal(total);
		}
		return data;
	}

	/**
	 * CRUD
	 * 
	 * @param user
	 *            用户信息
	 * @param operFlag
	 *            操作符
	 * @return
	 */
	@PostMapping(value = "/user/operUser/{oper}")
	public Result<Menu> operMenu(User user, @PathVariable("oper") Integer operFlag) {
		Result result = new Result<>();
		try {

			if (operFlag == 1) {
				// 新增
				if (Lang.isEmpty(user.getId())) {
				}
				// 更新
				else {
					userService.update(user);
				}
			}
			// 删除
			else if (operFlag == 2) {
			}
			result = ResultUtil.success();
		} catch (Exception e) {
			result = ResultUtil.fail(-1, "系统异常");
		}
		return result;
	}

}
