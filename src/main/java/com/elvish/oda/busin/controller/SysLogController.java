package com.elvish.oda.busin.controller;

import org.nutz.dao.QueryResult;
import org.nutz.dao.pager.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elvish.oda.busin.pojo.SysLog;
import com.elvish.oda.busin.service.SysLogService;
import com.elvish.oda.common.pojo.EasyUITableData;
import com.elvish.oda.common.pojo.Page;

@RestController
public class SysLogController {

	@Autowired
	private SysLogService sysLogService;

	@PostMapping(value = "/syslog/initSyslogTable")
	public EasyUITableData initSyslogTable(SysLog log, Page page) {
		EasyUITableData data = new EasyUITableData();
		QueryResult result = sysLogService.findAll(log, page);
		if (result != null) {
			Pager pager = result.getPager();
			data.setRows(result.getList());
			data.setTotal(pager.getRecordCount());
		}
		return data;
	}

}
