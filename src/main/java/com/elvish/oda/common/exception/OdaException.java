package com.elvish.oda.common.exception;

import com.elvish.oda.common.enums.ResultEnum;

public class OdaException extends RuntimeException {

	private Integer code;

	public OdaException(ResultEnum resultEnum) {
		super(resultEnum.getMsg());
		this.code = resultEnum.getCode();
	}

	public Integer getCode() {
		return code;
	}

}
