package com.elvish.oda.common.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elvish.oda.common.exception.OdaException;
import com.elvish.oda.common.pojo.Result;

/**
 * 异常处理类
 * 
 * @author Elvish
 *
 */
@ControllerAdvice
public class ExceptionHandle {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public Result handle(Exception e) {
		Result result = new Result();
		if (e instanceof OdaException) {
			OdaException pe = (OdaException) e;
			result.setCode(pe.getCode());
			result.setMsg(pe.getMessage());
		} else {
			result.setCode(-1);
			result.setMsg("未知错误");
		}
		return result;
	}

}
