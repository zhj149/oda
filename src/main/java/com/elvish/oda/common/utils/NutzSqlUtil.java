package com.elvish.oda.common.utils;

import java.util.List;

import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Criteria;
import org.nutz.dao.sql.Sql;

public class NutzSqlUtil {

	/**
	 * @desc 获取某个实体的集合
	 * @author zhu_longxiang
	 * @email 464773234@qq.com
	 * @date 2017年3月30日上午9:44:45
	 * @param sql
	 *            nutz sql对象
	 * @param dao
	 *            nutz dao对象
	 * @param clz
	 *            实体类型
	 * @return 实体数据集合
	 */
	public static <T> List<T> getEntities(Sql sql, Dao dao, Class<T> clz) {
		sql.setCallback(Sqls.callback.entities());
		sql.setEntity(dao.getEntity(clz));
		dao.execute(sql);
		return sql.getList(clz);
	}

	public static <T> List<T> getEntities(Dao dao, Criteria cri, Class<T> clz) {
		return dao.query(clz, cri, null);
	}

	public static <T> List<T> getEntities(Dao dao, Class<T> clz) {
		return dao.query(clz, null);
	}

	/**
	 * @desc 获取某个字段的单个结果
	 * @author zhu_longxiang
	 * @email 464773234@qq.com
	 * @date 2017年3月30日上午9:45:36
	 * @param sql
	 *            nutz sql对象
	 * @param dao
	 *            nutz dao对象
	 * @return 某个字段的单个结果
	 */
	public static String getString(Sql sql, Dao dao) {
		sql.setCallback(Sqls.callback.str());
		dao.execute(sql);
		return sql.getString();
	}

	/**
	 * @desc 获取某个实体数据
	 * @author zhu_longxiang
	 * @email 464773234@qq.com
	 * @date 2017年3月30日上午9:46:45
	 * @param sql
	 *            nutz sql对象
	 * @param dao
	 *            nutz dao对象
	 * @param clz
	 *            实体类型
	 * @return 单个实体数据
	 */
	public static <T> T getEntity(Sql sql, Dao dao, Class<T> clz) {
		sql.setCallback(Sqls.callback.entity());
		sql.setEntity(dao.getEntity(clz));
		dao.execute(sql);
		return sql.getObject(clz);
	}

	/**
	 * @desc 获取某个字段的所有结果
	 * @author zhu_longxiang
	 * @email 464773234@qq.com
	 * @date 2017年3月30日上午9:47:19
	 * @param sql
	 *            nutz sql对象
	 * @param dao
	 *            nutz dao对象
	 * @param name
	 *            字段名称
	 * @return 某个字段的所有结果集合
	 */
	public static List<String> getStringList(Sql sql, Dao dao) {
		sql.setCallback(Sqls.callback.strList());
		dao.execute(sql);
		return sql.getList(String.class);
	}

	/**
	 * 根据ID查找一条记录
	 * 
	 * @param dao
	 *            nutz dao对象
	 * @param clz
	 *            实体类型对应表
	 * @param id
	 *            记录id
	 * @return 记录数据
	 */
	public static <T> T findById(Dao dao, Class<T> clz, Integer id) {
		return dao.fetch(clz, id);
	}

	/**
	 * 插入一条记录
	 * 
	 * @param dao
	 *            nutz dao对象
	 * @param obj
	 *            实体对象
	 * @return 1成功
	 */
	public static <T> int save(Dao dao, T obj) {
		dao.insert(obj);
		return 1;
	}

	/**
	 * 根据id删除一条记录
	 * 
	 * @param dao
	 *            nutz dao对象
	 * @param clz
	 *            实体类型对应表
	 * @param id
	 *            记录id
	 * @return 1成功
	 */
	public static <T> int deleteById(Dao dao, Class<T> clz, Integer id) {
		dao.delete(clz, id);
		return 1;
	}
}
