package com.elvish.oda.common.pojo;

import org.nutz.json.Json;

public class EasyUITableData {

	private Object rows;
	private Integer total;

	public Object getRows() {
		return rows;
	}

	public void setRows(Object rows) {
		this.rows = rows;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return Json.toJson(this);
	}

}
