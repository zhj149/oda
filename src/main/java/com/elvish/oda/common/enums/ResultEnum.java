package com.elvish.oda.common.enums;

public enum ResultEnum {

	SUCCESS(0, "成功"), UNKOWN_ERROR(-1, "未知错误"),
	// 其他，方便管理code和描述对应关系,例如
	BABY(100, "我觉得你还是个婴儿"), GO_SCHOOL(101, "我觉得你应该在上幼儿园");

	Integer code;
	String msg;

	private ResultEnum(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

}
