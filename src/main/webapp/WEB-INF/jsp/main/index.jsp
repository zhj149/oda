<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh" style="overflow:hidden;">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
<%@ include file="/include/taglib.jsp"%>
</head>
<body>
	<script type="text/javascript">
		layer.config({
		    skin: 'demo-class'
		})
		layui.use('form', function() {
			var form = layui.form;
			form.render();
		});
	</script>
	<div id="oda-layout" class="easyui-layout" style="width:100%;height:100%;">
		<%@ include file="/include/top.jsp"%>
		<%@ include file="/include/menu.jsp"%>
		<%@ include file="/include/center.jsp"%>
		<%@ include file="/include/footer.jsp"%>
	</div>
</body>
</html>
