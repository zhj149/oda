<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="operRoleForm" class="layui-form">
				<input type="hidden" name="id" value="${role.id}">
				<div class="layui-form-item" pane>
					<label class="layui-form-label">角色名称</label>
					<div class="layui-input-block">
						<input type="text" name="roleName" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${role.roleName}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">备注</label>
					<div class="layui-input-block">
						<input type="text" name="remark" required
							lay-verify="required" autocomplete="off" class="layui-input"
							value="${role.remark}">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelOperRoleBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {

			$('#cancelOperRoleBtn').click(function() {
				closeDialog();
			})

			$('#operRoleForm').on('submit', function() {
				var $ids = [-1];
				$(this).ajaxSubmit({
					type : "post",
					url : "${ctx}/role/operRole/1",
					data : {
						'ids' : $ids
					},
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
						} else {
							operFail();
						}
					},
					error : function(data) {
						operFail();
					}
				})
				return false;
			})
		})
	</script>
</body>
</html>
