<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="operAreaForm" class="layui-form">
				<input type="hidden" name="id" value="${area.id}"> <input
					type="hidden" name="pid" value="${area.pid}">
				<div class="layui-form-item" pane>
					<label class="layui-form-label">地域名称</label>
					<div class="layui-input-block">
						<input type="text" name="name" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${area.name}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">地域编码</label>
					<div class="layui-input-block">
						<input type="text" name="code" required
							lay-verify="required" autocomplete="off" placeholder="请填写数字" class="layui-input"
							value="${area.code}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">地域类型</label>
					<div class="layui-input-block">
						<input type="text" name="type" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${area.type}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">备注</label>
					<div class="layui-input-block">
						<input type="text" name="remark"
							autocomplete="off" class="layui-input" value="${area.remark}">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelOperAreaBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {

			$('#cancelOperAreaBtn').click(function() {
				closeDialog();
			})

			$('#operAreaForm').on('submit', function() {
				$(this).ajaxSubmit({
					type : "post",
					url : "${ctx}/area/operArea/1",
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
						} else {
							operFail();
						}
					},
					error : function(data) {
						alert(1);
						operFail();
					}
				})
				return false;
			})
		})
	</script>
</body>
</html>
