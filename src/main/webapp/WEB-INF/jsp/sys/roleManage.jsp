<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh" style="overflow: hidden;">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="roleManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'center'">
			<div id="roleManageTB" class="oda-datagrid-toolbar">
				<div class="layui-btn-group">
					<button class="layui-btn layui-btn-xs" onclick="addRole();">
						<i class="layui-icon">&#xe608;</i> 添加
					</button>
					<button class="layui-btn layui-btn-xs" onclick="delRoles();">
						<i class="layui-icon">&#xe640;</i> 删除
					</button>
					<button class="layui-btn layui-btn-xs" onclick="assignPermissions();">
						<i class="layui-icon">&#xe627;</i> 分配权限
					</button>
				</div>
			</div>
			<table id="roleManageTable" class="easyui-datagrid"
				style="width: 100%; height: 100%;" rownumbers="true"
				pagination="true" pageSize="50" toolbar="#roleManageTB">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('#roleManageTable').datagrid({
			border : false,
			checkOnSelect : false,
			selectOnCheck : false,
			singleSelect : true,
			idField : "id",
			url : '${ctx}/role/initRoleTable',
			columns : [ [ {
				field : 'id',
				checkbox : true
			}, {
				field : 'roleName',
				title : '角色',
				width : 100
			}, {
				field : 'remark',
				title : '备注',
				width : 160
			} ] ],
			onDblClickRow : function(rowIndex, rowData){
				var id = rowData.id;		
				layer.open({
					type : 2,
					title : '角色信息修改',
					area : [ '450px', '220px' ],
					isOutAnim : false,
					content : '${ctx}/sys/roleManage/goOperRole?oper=2&id='+id,
					end : function() {
						$('#roleManageTable').datagrid('reload');
					}
				});
			}
		});

		function addRole() {
			layer.open({
				type : 2,
				title : '添加角色',
				area : [ '450px', '220px' ],
				isOutAnim : false,
				content : '${ctx}/sys/roleManage/goOperRole?oper=1',
				end : function() {
					$('#roleManageTable').datagrid('reload');
				}
			});
		}

		function delRoles() {
			var rows = getSelRows('roleManageTable');
			if (!rows || rows.length == 0) {
				layer.msg("你都不选，操作你妹啊？？？？");
				return;
			}
			var $ids = [];
			for (var i = 0; i < rows.length; i++) {
				$ids.push(rows[i].id);
			}
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				deleteByIds($ids);
			}, function() {
				closeDialog();
			});

		}

		function deleteByIds($ids) {
			$.ajax({
				type : "post",
				url : "${ctx}/role/operRole/2",
				async:false,
				dataType : "json",
				data : {
					'ids' : $ids
				},
				cache : false,
				success : function(data) {
					if (data.code == 0) {
						operSuccess();
						$('#roleManageTable').datagrid('reload');
					} else {
						operFail();
					}
				},
				error : function(data) {
					operFail();
				}
			})
		}
		
		function assignPermissions(){
			var rows = getSelRows('roleManageTable');
			if (!rows || rows.length == 0) {
				layer.msg("你都不选，操作你妹啊？？？？");
				return;
			}
			
			if (rows.length > 1) {
				layer.msg("只能选择一条记录");
				return;
			}
			
			layer.open({
				type : 2,
				title : '分配权限',
				area : [ '450px', '70%' ],
				isOutAnim : false,
				content : '${ctx}/sys/roleManage/assignPermissions?roleId='+rows[0].id,
				end : function() {
					$('#roleManageTable').datagrid('reload');
				}
			});
			
			
			
		}
	</script>
</body>
</html>
