<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="operMenuForm" class="layui-form">
				<input type="hidden" name="id" value="${menu.id}"> <input
					type="hidden" name="pid" value="${menu.pid}">
				<div class="layui-form-item" pane>
					<label class="layui-form-label">菜单名称</label>
					<div class="layui-input-block">
						<input type="text" name="menuName" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${menu.menuName}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">菜单编码</label>
					<div class="layui-input-block">
						<input type="text" name="permissionCode" required
							lay-verify="required" autocomplete="off" class="layui-input"
							value="${menu.permissionCode}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">菜单URL</label>
					<div class="layui-input-block">
						<input type="text" name="url" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${menu.url}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">菜单图标</label>
					<div class="layui-input-block">
						<input type="text" name="icon"
							autocomplete="off" class="layui-input" value="${menu.icon}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">排序</label>
					<div class="layui-input-block">
						<input type="text" name="sortnum" required lay-verify="required"
							autocomplete="off" placeholder="请填写数字" class="layui-input" value="${menu.sortnum}">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelOperMenuBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {

			$('#cancelOperMenuBtn').click(function() {
				closeDialog();
			})

			$('#operMenuForm').on('submit', function() {
				$(this).ajaxSubmit({
					type : "post",
					url : "${ctx}/menu/operMenu/1",
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
						} else {
							operFail();
						}
					},
					error : function(data) {
						operFail();
					}
				})
				return false;
			})
		})
	</script>
</body>
</html>
