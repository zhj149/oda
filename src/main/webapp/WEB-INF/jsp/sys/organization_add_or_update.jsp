<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="operOrganizationForm" class="layui-form">
				<input type="hidden" name="id" value="${organization.id}">
				<div class="layui-form-item" pane>
					<label class="layui-form-label">机构名称</label>
					<div class="layui-input-block">
						<input type="text" name="name" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${organization.name}">
					</div>
				</div>
				
				<div class="layui-form-item" pane>
					<label class="layui-form-label">所属地域</label>
					<div class="layui-input-block">
						<select id="areaTree" name="areaId" class="easyui-combotree" style="width:310px;height:37px;"
						    data-options="url:'${ctx}/area/initAreaTreeTable',required:true">
						</select>
					</div>
				</div>
				
				<div class="layui-form-item" pane>
					<label class="layui-form-label">机构编码</label>
					<div class="layui-input-block">
						<input type="text" name="code" required
							lay-verify="required" autocomplete="off" placeholder="请填写数字" class="layui-input"
							value="${organization.code}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">机构类型</label>
					<div class="layui-input-block">
						<input type="text" name="type" required lay-verify="required"
							autocomplete="off" class="layui-input" value="${organization.type}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">备注</label>
					<div class="layui-input-block">
						<input type="text" name="remark"
							autocomplete="off" class="layui-input" value="${organization.remark}">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelOperOrganizationBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<input type="hidden" id="areaId" value="${organization.areaId}"/>

	<script type="text/javascript">
		$(function() {
			
			var areaId = $('#areaId').val();
			if(areaId){
				$('#areaTree').combotree('setValue', areaId);
			}
			$('#cancelOperOrganizationBtn').click(function() {
				closeDialog();
			})

			$('#operOrganizationForm').on('submit', function() {
				var $ids = [-1];
				$(this).ajaxSubmit({
					type : "post",
					url : "${ctx}/organization/operOrganization/1",
					data : {
						'ids' : $ids
					},
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
						} else {
							operFail();
						}
					},
					error : function(data) {
						alert(1);
						operFail();
					}
				})
				return false;
			})
		})
	</script>
</body>
</html>
