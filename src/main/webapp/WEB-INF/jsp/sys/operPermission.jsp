<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="operPermissionForm" class="layui-form">
				<input type="hidden" name="id" value="${permission.id}"> <input
					type="hidden" id="pid" name="pid" value="${permission.pid}">
				<input type="hidden" id="permissionName" name="permissionName"
					value="${permission.permissionName}">
				<div class="layui-form-item">
					<label class="layui-form-label">权限类型</label>
					<div class="layui-input-block">
						<input type="radio" name="type" value="1" title="菜单" checked
							lay-filter="change"> <input type="radio" name="type"
							value="2" title="按钮" lay-filter="change">
					</div>
				</div>
				<div id="permissionTreeDiv" class="layui-form-item"
					style="display: none;">
					<label class="layui-form-label">权限树</label>
					<div class="layui-input-block">
						<select id="permissionTree" name="menuid" class="easyui-combotree"
							style="width: 310px; height: 37px;"
							data-options="url:'${ctx}/permission/initPermissionTreeTable',required:true">
						</select>
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">权限名称</label>
					<div class="layui-input-block">
						<input type="text" id="btnName" name="permissionName" required
							lay-verify="required" autocomplete="off" class="layui-input"
							value="${permission.permissionName}">
					</div>
				</div>
				<div class="layui-form-item" pane>
					<label class="layui-form-label">权限编码</label>
					<div class="layui-input-block">
						<input type="text" id="permissionCode" name="permissionCode"
							required lay-verify="required" autocomplete="off"
							class="layui-input" value="${permission.permissionCode}">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelOperMenuBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<input type="hidden" id="type" value="${permission.type}">

	<script type="text/javascript">
		$(function() {
			var value = 1;

			layui.use('form', function() {
				var form = layui.form;
				$('.layui-select-title').hide();

				form.on('radio(change)', function(data) {
					value = data.value;
					if (value == 2) {
						$('#permissionTreeDiv').show();
					} else {
						$('#permissionTreeDiv').hide();
					}
				});
			});

			var permissionName = $('#permissionName').val();
			if (permissionName) {
				$('#permissionTree').combotree('setValue', permissionName);
				// 修改初始化
				var type = $('#type').val();
				if (type == 2) {
					$('input:radio[value="1"]').attr('disabled', 'disabled');
					$('input:radio[value="2"]').attr('checked', 'true');
					$('#permissionTreeDiv').show();
				} else {
					$('input:radio[value="2"]').attr('disabled', 'disabled');
				}
			}

			$('#cancelOperMenuBtn').click(function() {
				closeDialog();
			})

			$('#operPermissionForm').on('submit', function() {
				var t = $('#permissionTree').combotree('tree');
				var n = t.tree('getSelected');
				if (n) {
					// 重新选择赋值
					$('#permissionName').attr('value', n.text);
				}
				// 如果是按钮，则重新封装数据
				if (value == 2 || type == 2) {
					// pid
					if (n) {
						// 重新选择赋值
						$('#pid').attr('value', n.id);
					}
				}

				$('#permissionName').remove();
				
				$(this).ajaxSubmit({
					type : "post",
					url : "${ctx}/permission/operPermission/1",
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
						} else {
							operFail();
						}
					},
					error : function(data) {
						operFail();
					}
				})
				return false;
			})
		})
	</script>
</body>
</html>
