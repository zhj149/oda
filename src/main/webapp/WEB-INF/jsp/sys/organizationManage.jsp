<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="organizationManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'west'" style="width: 15%; height: 100%;">
			<div id="leftAreaTree" class="easyui-panel" style="height: 100%;">
			</div>
		</div>
		<div data-options="region:'center'" style="height: 100%; width: 85%">
			<div id="organizationManageTB">
				<a onclick="addOrganization('-1')" class="easyui-linkbutton"
					iconCls="icon-add" plain="true">新增</a> <a onclick="delOrganization();"
					class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
			</div>
			<table id="organizationManageTable" class="easyui-datagrid"
				style="height: 100%;" rownumbers="true"
				pagination="true"
				toolbar="#organizationManageTB">
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#leftAreaTree').tree({
				url : '${ctx}/area/initAreaTreeTable',
				lines : true,
				onSelect : function(node) {
					// 刷新表格
					$('#organizationManageTable').datagrid('options').queryParams = {areaId:node.id};      
					$('#organizationManageTable').datagrid('reload');
				}
			});
			
			$('#organizationManageTable').datagrid({
				border : false,
				checkOnSelect : false,
				selectOnCheck : false,
				singleSelect : true,
				idField : "id",
				treeField : "name",
				url : '${ctx}/organization/initOrganizationTable',
				columns : [ [ {
					field : 'id',
					checkbox : true
				}, {
					field : 'name',
					title : '机构名称',
					width : 200
				}, {
					field : 'code',
					title : '机构编码',
					width : 160
				}, {
					field : 'type',
					title : '机构类型',
					width : 160
				}, {
					field : 'remark',
					title : '备注',
					width : 100
				}, {
					field : 'operate',
					title : '操作',
					width : 100,
					align : 'center',
					formatter : function(value, row, index) {
						var id = row.id;
						var btn = "";
						btn += "<a class='updateCls'  onclick=\"updateOrganizationById(" + id + ");\">修改</a>    ";
						return btn;
					}
				} ] ],
				onLoadSuccess : function(data) {
					$('.updateCls').linkbutton({
						text : '修改',
						plain : true,
						iconCls : 'icon-edit'
					});
				}
			});
		});

		function addOrganization(id) {
			layer.open({
				type : 2,
				title : '添加机构',
				area : [ '450px', '450px' ],
				isOutAnim : false,
				content : '${ctx}/sys/organizationManage/goOperOrganization/add/' + id,
				end : function() {
					$('#organizationManageTable').datagrid('reload');
				}
			});
		}
		
		function updateOrganizationById(id){
			layer.open({
				type : 2,
				title : '菜单修改',
				area : [ '450px', '450px' ],
				isOutAnim : false,
				content : '${ctx}/sys/organizationManage/goOperOrganization/update/' + id,
				end : function() {
					$('#organizationManageTable').datagrid('reload');
				}
			});
		}
		
		function delOrganization(){
			var rows = getSelRows('organizationManageTable');
			if (!rows || rows.length == 0) {
				layer.msg("你都不选，操作你妹啊？？？？");
				return;
			}
			var ids = [];
			for (var i = 0; i < rows.length; i++) {
				ids.push(rows[i].id);
			}
			
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				deleteByIds(ids);
			}, function() {
				closeDialog();
			});
			
			function deleteByIds($ids) {
				$.ajax({
					type : "post",
					url : "${ctx}/organization/operOrganization/2",
					async:false,
					dataType : "json",
					data : {
						'ids' : $ids
					},
					cache : false,
					success : function(data) {
						if (data.code == 0) {
							operSuccess();
							$('#organizationManageTable').datagrid('reload');
						} else {
							operFail();
						}
					},
					error : function(data) {
						operFail();
					}
				})
			}
			
			
		}
	</script>
</body>
</html>
