<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh" style="overflow: hidden;">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="areaManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'center'">
			<div id="areaManageTB">
				<a href="#" class="easyui-linkbutton" iconCls="icon-add"
					plain="true" onclick="addArea('0',-1);">新增</a>
			</div>
			<table id="areaManageTable" style="width: 100%; height: 100%;"
				toolbar="#areaManageTB" rownumbers="true">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('#areaManageTable').treegrid({
			border : false,
			singleSelect : true,
			idField : "id",
			treeField : "name",
			url : '${ctx}/area/initAreaTreeTable',
			columns : [ [ {
				field : 'name',
				title : '区域名称',
				width : 200
			}, {
				field : 'code',
				title : '区域编码',
				width : 160
			}, {
				field : 'type',
				title : '区域类型',
				width : 160
			}, {
				field : 'remark',
				title : '备注',
				width : 100
			}, {
				field : 'operate',
				title : '操作',
				width : 300,
				align : 'center',
				formatter : function(value, row, index) {
					var id = row.id;
					var btn = "";
					btn += "<a class='updateCls'  onclick=\"updateAreaById(" + id + ");\">修改</a>    ";
					btn += "<a class='delCls'  onclick=\"deleteAreaById(" + id + ");\">刪除</a>    ";
					btn += "<a class='addCls'  onclick=\"addArea(" + id + ",'1')\">添加下级区域</a>    ";
					return btn;
				}
			} ] ],
			onLoadSuccess : function(data) {
				$('.updateCls').linkbutton({
					text : '修改',
					plain : true,
					iconCls : 'icon-edit'
				});
				$('.delCls').linkbutton({
					text : '刪除',
					plain : true,
					iconCls : 'icon-remove'
				});
				$('.addCls').linkbutton({
					text : '添加下级区域',
					plain : true,
					iconCls : 'icon-add'
				});
			}
		});
		
		function updateAreaById(id){
			layer.open({
				type : 2,
				title : '菜单修改',
				area : [ '450px', '400px' ],
				isOutAnim : false,
				content : '${ctx}/sys/areaManage/goOperArea/update/1/' + id,
				end : function() {
					$('#areaManageTable').treegrid('reload');
				}
			});
		}

		function addArea(id, level) {
			var $title = id == '-1' ? '添加菜单' : '添加下级菜单'
			layer.open({
				type : 2,
				title : $title,
				area : [ '450px', '330px' ],
				isOutAnim : false,
				content : '${ctx}/sys/areaManage/goOperArea/add/' + level + '/'
						+ id,
				end : function() {
					$('#areaManageTable').treegrid('reload');
				}
			});
		}
		
		function deleteAreaById(id) {
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				deleteById(id);
			}, function() {
				closeDialog();
			});
		}

		function deleteById(id) {
			$.ajax({
				type : 'post',
				url : "${ctx}/area/operArea/2",
				data : {
					id : id
				},
				success : function(data) {
					if (data.code == 0) {
						operSuccess();
						// $('#areaManageTable').treegrid('reload');
						refreshTab();
					} else {
						operFail();
					}
				},
				error : function(data) {
					operFail();
				}
			})
		}
	</script>
</body>
</html>
