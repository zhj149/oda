<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_easyui.jsp"%>
<%@ include file="/include/header_other.jsp"%>
<%@ include file="/include/taglib.jsp"%>
</head>
<body>
	<div class="layui-container" style="margin-top: 15px;">
		<div class="layui-row">
			<form id="chooseRoleForm" class="layui-form">
				<div class="layui-form-item">
					<label class="layui-form-label">角色选择</label>
					<div class="layui-input-block">
						<select name="roles" lay-verify="required">
							<c:forEach items="${roles}" var="role">
								<option value="${role.id}">${role.roleName}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit>保存</button>
						<button id="cancelChooseRoleBtn" class="layui-btn">取消</button>
					</div>
				</div>
			</form>
			<input type="hidden" id="userId" value="${userId}" />
		</div>
	</div>

	<script type="text/javascript">
		$(function() {

			layui.use('form', function() {
				var form = layui.form;
				form.render();
			});

			$('#cancelChooseRoleBtn').click(function() {
				closeDialog();
			})

			$('#chooseRoleForm').on(
					'submit',
					function() {
						var id = $('select[name="roles"]').find(
								"option:selected").attr('value');
						var userId = $('#userId').val();
						if (!id) {
							layer.msg("你都不选，操作你妹啊？？？？");
							return;
						}
						$(this).ajaxSubmit({
							type : "post",
							url : "${ctx}/role/chooseRole",
							async : false,
							dataType : "json",
							data : {
								id : id,
								userId : userId
							},
							cache : false,
							success : function(data) {
								if (data.code == 0) {
									operSuccess();
								} else {
									operFail();
								}
							},
							error : function(data) {
								operFail();
							}
						})
						return false;
					})
		})
	</script>
</body>
</html>
