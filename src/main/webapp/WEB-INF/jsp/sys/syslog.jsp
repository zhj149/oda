<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh" style="overflow: hidden;">
<head>
<%@ include file="/include/header_base.jsp"%>
<%@ include file="/include/header_other.jsp"%>
</head>
<body>
	<div id="syslogLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div id="syslogTB" class="oda-datagrid-toolbar">
			<form id="syslog_search" class="layui-form">
				<div class="layui-form-item oda-form-item">
					<label class="layui-form-label oda-form-label">用户名</label>
					<div class="layui-input-inline oda-input-inline">
						<input type="text" name="username" placeholder=""
							autocomplete="off" class="layui-input oda-input">
					</div>
					<label class="layui-form-label oda-form-label">请求URL</label>
					<div class="layui-input-inline oda-input-inline">
						<input type="text" name="url" placeholder="" autocomplete="off"
							class="layui-input oda-input">
					</div>
					<label class="layui-form-label oda-form-label">请求方式</label>
					<div class="layui-input-inline oda-input-inline">
						<input type="text" id="method" name="method" placeholder="" autocomplete="off"
							class="layui-input oda-input">
					</div>
					<button lay-submit class="layui-btn layui-btn-xs">
						<i class="layui-icon">&#xe615;</i> 搜索
					</button>
				</div>
			</form>
		</div>
		<div data-options="region:'center'">
			<table id="syslogTable" style="width: 100%; height: 100%;"
				rownumbers="true" pagination="true" pageSize="50" toolbar="#syslogTB">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('#syslogTable').datagrid({
			border : false,
			checkOnSelect : false,
			selectOnCheck : false,
			singleSelect : true,
			idField : "id",
			url : '${ctx}/syslog/initSyslogTable',
			columns : [ [ {
				field : 'id',
				checkbox : true
			}, {
				field : 'username',
				title : '用户名',
				width : 50
			}, {
				field : 'date',
				title : '操作时间',
				width : 140,
				formatter : dateFormat
			}, {
				field : 'url',
				title : '请求url',
				width : 160
			}, {
				field : 'method',
				title : '请求方式',
				width : 60
			}, {
				field : 'ip',
				title : '请求地址',
				width : 120
			}, {
				field : 'classMethod',
				title : '调用类方法',
				width : 400
			}, {
				field : 'args',
				title : '请求参数',
				width : 160
			} ] ]
		});
		
		$('#syslog_search').on('submit', function() {
			var params = queryParamByFormId("syslog_search");
		   	$('#syslogTable').datagrid('options').queryParams = params;        
			$('#syslogTable').datagrid('reload');
			return false;
		})
	</script>
</body>
</html>
