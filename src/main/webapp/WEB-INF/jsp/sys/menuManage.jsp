<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="menuManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'center'">
			<div id="menuManageTB">
				<a onclick="addMenu('-1','0');" class="easyui-linkbutton"
					iconCls="icon-add" plain="true">新增</a>
			</div>
			<table id="menuManageTable" style="width: 100%; height: 100%;"
				toolbar="#menuManageTB">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('#menuManageTable')
				.treegrid(
						{
							border : false,
							singleSelect : true,
							idField : "id",
							treeField : "menuName",
							url : '${ctx}/menu/initMenuTreeTable',
							columns : [ [
									{
										field : 'menuName',
										title : '名称',
										width : 200
									},
									{
										field : 'permissionCode',
										title : '编码',
										width : 100
									},
									{
										field : 'url',
										title : 'URL',
										width : 200
									},
									{
										field : 'icon',
										title : '图标',
										width : 80
									},
									{
										field : 'sortnum',
										title : '排序',
										width : 50
									},
									{
										field : 'operate',
										title : '操作',
										width : 300,
										align : 'center',
										formatter : function(value, row, index) {
											var btn = "";
											var parentId = row.pid;
											if (parentId) {
												btn += "<a class='updateCls'  onclick='updateMenuById("
														+ row.id
														+ ")'>修改</a>    ";
												btn += "<a class='delCls'  onclick='deleteMenuById("
														+ row.id
														+ ")'>刪除</a>    ";
											} else {
												btn += "<a class='updateCls'  onclick='updateMenuById("
														+ row.id
														+ ")'>修改</a>    ";
												btn += "<a class='delCls'  onclick='deleteMenuById("
														+ row.id
														+ ")'>刪除</a>    ";
												btn += "<a class='addCls'  onclick='addMenu("
														+ row.id
														+ ','
														+ 1
														+ ");'>添加下级菜单</a>    ";
											}
											return btn;
										}
									} ] ],
							onLoadSuccess : function(data) {
								$('.updateCls').linkbutton({
									text : '修改',
									plain : true,
									iconCls : 'icon-edit'
								});
								$('.delCls').linkbutton({
									text : '刪除',
									plain : true,
									iconCls : 'icon-remove'
								});
								$('.addCls').linkbutton({
									text : '添加下级菜单',
									plain : true,
									iconCls : 'icon-add'
								});
							}
						});

		function addMenu(id, level) {
			var $title = id == '-1' ? '添加菜单' : '添加下级菜单'
			layer.open({
				type : 2,
				title : $title,
				area : [ '450px', '330px' ],
				isOutAnim : false,
				content : '${ctx}/sys/menuManage/goOperMenu/add/' + level + '/'
						+ id,
				end : function() {
					$('#menuManageTable').treegrid('reload');
				}
			});
		}

		function deleteMenuById(id) {
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				deleteById(id);
			}, function() {
				closeDialog();
			});
		}

		function deleteById(id) {
			$.ajax({
				type : 'post',
				url : "${ctx}/menu/operMenu/2",
				data : {
					id : id
				},
				success : function(data) {
					if (data.code == 0) {
						operSuccess();
						refreshTab();
						// $('#menuManageTable').treegrid('reload');
					} else {
						operFail();
					}
				},
				error : function(data) {
					operFail();
				}
			})
		}

		function updateMenuById(id) {
			layer.open({
				type : 2,
				title : '菜单修改',
				area : [ '450px', '400px' ],
				isOutAnim : false,
				content : '${ctx}/sys/menuManage/goOperMenu/update/1/' + id,
				end : function() {
					$('#menuManageTable').treegrid('reload');
				}
			});
		}
	</script>
</body>
</html>
