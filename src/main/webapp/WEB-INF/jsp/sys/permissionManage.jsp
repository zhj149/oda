<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="menuManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'center'">
			<div id="permissionManageTB">
				<a onclick="addPermission(-1,0);" class="easyui-linkbutton"
					iconCls="icon-add" plain="true">新增</a>
			</div>
			<table id="permissionManageTable" style="width: 100%; height: 100%;"
				toolbar="#permissionManageTB">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$('#permissionManageTable')
				.treegrid(
						{
							border : false,
							singleSelect : true,
							idField : "id",
							treeField : "permissionName",
							url : '${ctx}/permission/initPermissionTreeTable',
							columns : [ [
									{
										field : 'permissionName',
										title : '名称',
										width : 200
									},
									{
										field : 'permissionCode',
										title : '编码',
										width : 100
									},
									{
										field : 'type',
										title : '类型',
										width : 100,
										formatter : function(value, row, index) {
											if(value == 1){ return "菜单"}
											else if(value == 2){return "按钮"}
										}
									},
									{
										field : 'operate',
										title : '操作',
										width : 300,
										align : 'center',
										formatter : function(value, row, index) {
											var btn = "";
											btn += "<a class='updateCls'  onclick='updatePermissionById("
													+ row.id + ")'>修改</a>    ";
											btn += "<a class='delCls'  onclick='deletePermissionById("
													+ row.id + ")'>刪除</a>    ";
											btn += "<a class='addCls'  onclick='addPermission("
													+ row.id
													+ ','
													+ 1
													+ ");'>添加下级权限</a>    ";
											return btn;
										}
									} ] ],
							onLoadSuccess : function(data) {
								$('.updateCls').linkbutton({
									text : '修改',
									plain : true,
									iconCls : 'icon-edit'
								});
								$('.delCls').linkbutton({
									text : '刪除',
									plain : true,
									iconCls : 'icon-remove'
								});
								$('.addCls').linkbutton({
									text : '添加下级权限',
									plain : true,
									iconCls : 'icon-add'
								});
							}
						});

		function addPermission(id, level) {
			layer.open({
				type : 2,
				title : '添加权限',
				area : [ '450px', '380px' ],
				isOutAnim : false,
				content : '${ctx}/sys/permissionManage/goOperPermission/add/'
						+ level + '/' + id,
				end : function() {
					$('#permissionManageTable').treegrid('reload');
				}
			});
		}

		function deletePermissionById(id) {
			layer.confirm('确定要删除吗？', {
				btn : [ '确定', '取消' ]
			}, function() {
				deleteById(id);
			}, function() {
				closeDialog();
			});
		}

		function deleteById(id) {
			$.ajax({
				type : 'post',
				url : "${ctx}/permission/operPermission/2",
				data : {
					id : id
				},
				success : function(data) {
					if (data.code == 0) {
						operSuccess();
						$('#permissionManageTable').treegrid('reload');
					} else {
						operFail();
					}
				},
				error : function(data) {
					operFail();
				}
			})
		}

		function updatePermissionById(id) {
			layer.open({
				type : 2,
				title : '权限修改',
				area : [ '450px', '380px' ],
				isOutAnim : false,
				content : '${ctx}/sys/permissionManage/goOperPermission/update/1/'
						+ id,
				end : function() {
					$('#permissionManageTable').treegrid('reload');
				}
			});
		}
	</script>
</body>
</html>
