<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh" style="overflow: hidden;">
<head>
<%@ include file="/include/header_base.jsp"%>
</head>
<body>
	<div id="userManageLayout" class="easyui-layout"
		style="width: 100%; height: 100%;">
		<div data-options="region:'center'">
			<div id="userManageTB" class="oda-datagrid-toolbar">
				<div class="layui-btn-group">
					<button class="layui-btn layui-btn-xs" onclick="operColumn()">
						<i class="layui-icon">&#xe671;</i> 设置
					</button>
					<button class="layui-btn layui-btn-xs">
						<i class="layui-icon">&#xe608;</i> 添加
					</button>
					<button class="layui-btn layui-btn-xs">
						<i class="layui-icon">&#xe640;</i> 删除
					</button>
					<button class="layui-btn layui-btn-xs" onclick="assignRoles();">
						<i class="layui-icon">&#xe627;</i> 分配角色
					</button>
					<button class="layui-btn layui-btn-xs"
						onclick="updateUsersStatus();">
						<i class="layui-icon">&#xe6af;</i> 启用
					</button>
					<button class="layui-btn layui-btn-xs"
						onclick="updateUsersStatus();">
						<i class="layui-icon">&#xe69c;</i> 禁用
					</button>
				</div>
			</div>
			<table id="userManageTable" class="easyui-datagrid"
				style="width: 100%; height: 100%;" rownumbers="true"
				pagination="true" pageSize="50" toolbar="#userManageTB">
			</table>
		</div>
	</div>

	<script type="text/javascript">
		function statusFormat(value, row, index) {
			if (value == '0') {
				return '<span id="'
						+ row.id
						+ '" onclick="updateUserStatus(this);" class="layui-badge layui-bg-green">启用</span>';
			} else {
				return '<span id="'
						+ row.id
						+ '" onclick="updateUserStatus(this);" class="oda-cursor layui-badge">停用</span>';
			}
		}

		$('#userManageTable').datagrid({
			border : false,
			checkOnSelect : false,
			selectOnCheck : false,
			singleSelect : true,
			idField : "id",
			url : '${ctx}/user/initUserTable',
			columns : [ [ {
				field : 'id',
				checkbox : true
			}, {
				field : 'name',
				title : '姓名',
				width : 100
			}, {
				field : 'roleName',
				title : '角色',
				width : 100
			}, {
				field : 'loginTime',
				title : '登录时间',
				width : 160,
				formatter : dateFormat
			}, {
				field : 'delflag',
				title : '状态',
				width : 44,
				formatter : statusFormat
			}, {
				field : 'remark',
				title : '备注',
				width : 160
			} ] ]
		});

		$('#userManageSet').datagrid({
			border : false,
			checkOnSelect : false,
			selectOnCheck : false,
			singleSelect : true,
			idField : "id",
			columns : [ [ {
				field : 'id',
				checkbox : true
			}, {
				field : 'columnName',
				title : '列名',
				width : 100
			} ] ]
		});

		/**
		 * @method : 批量操作角色
		 */
		function assignRoles() {
			var rows = getSelRows('userManageTable');
			if (!rows || rows.length == 0) {
				layer.msg("你都不选，操作你妹啊？？？？");
				return;
			}
			if (rows.length > 1) {
				layer.msg("只能选择一条记录");
				return;
			}
			// TODO 给用户分配角色

			layer.open({
				type : 2,
				title : '角色选择',
				area : [ '450px', '220px' ],
				isOutAnim : false,
				content : '${ctx}/sys/roleManage/chooseRole?userId='
						+ rows[0].id,
				end : function() {
					$('#userManageTable').datagrid('reload');
				}
			});

		}

		/**
		 * @method : 批量操作修改用户状态
		 * @param ：操作状态标识
		 */
		function updateUsersStatus() {
			var rows = getSelRows('userManageTable');
			if (!rows || rows.length == 0) {
				layer.msg("你都不选，操作你妹啊？？？？");
				return;
			}
			var ids = [];
			for (var i = 0; i < rows.length; i++) {
				ids.push(rows[i].id);
			}
			alert(ids);
			// TODO 0启用or1禁用
		}

		/**
		 * @method : 表格单行操作修改用户状态
		 * @param ：当前span dom
		 */
		function updateUserStatus(target) {
			var id = $(target).attr('id');
			$.ajax({
				type : 'post',
				url : "${ctx}/user/operUser/1",
				data : {
					id : id,
					delflag : $(target).text() == '停用' ? 0 : 1
				},
				success : function(data) {
					if (data.code == 0) {
						operSuccess();
						$('#userManageTable').datagrid('reload');
					} else {
						operFail();
					}
				},
				error : function(data) {
					operFail();
				}
			})
		}

		function operColumn() {
			/* layer.open({
				type : 1,
				title : '表格列显示设置',
				area : [ '200px', '420px' ],
				shadeClose : true,
				content : html
			}); */
			// $('#userManageTable').datagrid('hideColumn', 'remark');
		}
	</script>
</body>
</html>
