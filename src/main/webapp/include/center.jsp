<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<div data-options="region:'center'" style="height: 100%">
	<div id="tt" class="easyui-tabs" data-options="border:false,fit:true">
		<div title="首页" data-options="iconCls:'fa fa-home'">首页的内容</div>
	</div>
	<div id="menu" class="easyui-menu" style="width: 150px;">
		<div id="m-refresh" data-options="iconCls:''">刷新</div>
		<div id="m-closeall" data-options="iconCls:''">全部关闭</div>
		<div id="m-closeother" data-options="iconCls:''">关闭其他</div>
		<div id="m-close" data-options="iconCls:''">关闭</div>
	</div>
</div>

