<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<div class="oda-header oda-header-bg" data-options="region:'north',border:false">
	<div class="oda-header-left">
         <h1>后台管理系统</h1>
    </div>
    <div class="oda-header-right">
     	 <p><strong><shiro:principal property="name"/></strong>，欢迎您！</p>
         <p class="oda-header-right-m-t"><a href="javascript:alert('帮不了你')">帮助中心</a>|<a href="${ctx}/sys/logout">安全退出</a></p>
    </div>
</div>
 
