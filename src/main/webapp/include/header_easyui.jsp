<%@ page pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/static/plugins/easyui/themes/default/easyui.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/plugins/easyui/themes/icon.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/plugins/easyui/themes/super/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/static/plugins/easyui/themes/super/superBlue.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/static/plugins/easyui/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/easyui/jquery.easyui.min.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/easyui/easyui-lang-zh_CN.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/easyui/themes/super/super.js"></script>
<script src="${pageContext.request.contextPath}/static/plugins/easyui/themes/super/superDemo.js"></script>