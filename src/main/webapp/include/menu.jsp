<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<div data-options="region:'west',split:true" title="系統菜单"
	style="width: 190px;">
	<div class="easyui-accordion"
		data-options="border:false,fit:true,selected:true">
		<c:forEach items="${menus}" var="menu">
			<c:if test="${menu.pid == null}">
				<div title="${menu.menuName}" data-options="iconCls:'${menu.icon}'">
					<c:if test="${menu.children != ''}">
						<ul>
							<c:forEach items="${menu.children}" var="childrenMenu">
								<li data-url='${ctx}${childrenMenu.url}'>${childrenMenu.menuName}</li>
							</c:forEach>
						</ul>
					</c:if>
				</div>
			</c:if>
		</c:forEach>
	</div>
</div>