$(function() {
	// 初始化主题
	var initTheme = function(themeName) {
		if(themeName == null) {
			themeName = $('#themeCss').attr('href').split('/').pop().split('.css')[0];
			// 添加勾选状态
			$(".themeItem ul li").removeClass('themeActive');
			$('.themeItem ul li .' + themeName).parent().addClass('themeActive');
			return;
		}
		var themeUrl = $('#themeCss').attr('href').split('/');
		themeUrl.pop();
		$('#themeCss').after('<link rel="stylesheet" href="' + themeUrl.join('/') + '/' + themeName + '.css" id="themeCss">');
		$('#themeCss').remove();

		// 添加勾选状态
		$(".themeItem ul li").removeClass('themeActive');
		$('.themeItem ul li .' + themeName).parent().addClass('themeActive');
	}

	// initTheme(localStorage.getItem('superTheme'));

	// 左侧导航分类选中样式
	$(".panel-body .accordion-body>ul").on('click', 'li', function() {
		$(this).siblings().removeClass('super-accordion-selected');
		$(this).addClass('super-accordion-selected');

		//新增一个选项卡
		var tabUrl = $(this).data('url');
		var tabTitle = $(this).text();
		//tab是否存在
		if($("#tt").tabs('exists', tabTitle)) {
			$("#tt").tabs('select', tabTitle);
		} else {
			var content = '<iframe scrolling="auto" frameborder="0"  src="' + tabUrl + '" style="width:100%;height:99%;"></iframe>';
			$('#tt').tabs('add', {
				title: tabTitle,
				//content: content,
				href: tabUrl,
				closable: true
			});
		}
	});
	
	$("#tt").tabs({
        onContextMenu : function(e) {
            /* 选中当前触发事件的选项卡 */
            var subtitle = $(this).text();
            $('#tt').tabs('select', subtitle);
            //显示快捷菜单
            e.preventDefault();
            //阻止冒泡
            $('#menu').menu('show', {
                left : e.pageX,
                top : e.pageY
            });
            return false;
        }
    });
	
	//关闭所有
    $("#m-closeall").click(function () {
        $(".tabs li").each(function (i, n) {
            var title = $(n).text();
            if (title != '首页') {//非主页全部关闭
                $('#tt').tabs('close', title);
            }
        });
    });
	//除当前之外关闭所有
    $("#m-closeother").click(function () {
        var currTab = $('#tt').tabs('getSelected');
        currTitle = currTab.panel('options').title;
        $(".tabs li").each(function (i, n) {
            var title = $(n).text();
            if (currTitle != title && title != '首页') {//除本页和主页以外全部关闭
                $('#tt').tabs('close', title);
            }
        });
    });
	
	//刷新
    $("#m-refresh").click(function () {
        var currTab = $('#tt').tabs('getSelected');    
        var url = $(currTab.panel('options')).attr('href');                
        var title = $(currTab.panel('options')).attr('title');  
        var refeshTab = $('#tt').tabs("getTab", title);
        if (currTab != refeshTab) {
            tabPanel.tabs("select", title);
            currTab = refeshTab;
          }
        /* 重新设置该标签 */
        $('#tt').tabs('update', {
            tab: currTab,
            options: {
                // content: '<iframe src="' + url + '"  style="height: 100%; width: 100%;" ></iframe>'
            	 href: url
            }
        })
        currTab.panel('refresh');
    });
    
    //关闭当前
    $("#m-close").click(function () {
        var currTab = $('#tt').tabs('getSelected');
        currTitle = currTab.panel('options').title;
        $('#tt').tabs('close', currTitle);
    });

	// 设置按钮的下拉菜单
	$('.super-setting-icon').on('click', function() {
		$('#mm').menu('show', {
			top: 50,
			left: document.body.scrollWidth - 160
		});
	});

	// 修改主题
	$('#themeSetting').on('click', function() {
		var themeWin = $('#win').dialog({
			width: 460,
			height: 260,
			modal: true,
			title: '主题设置',
			buttons: [{
				text: '保存',
				id: 'btn-sure',
				handler: function() {
					themeWin.panel('close');
					// css
					var themeName = $(".themeItem ul li.themeActive>div").attr('class');
					initTheme(themeName);
					localStorage.setItem('superTheme', themeName);
				}
			}, {
				text: '关闭',
				handler: function() {
					themeWin.panel('close');
				}
			}],
			onOpen: function() {
				$(".themeItem").show();
			}
		});
	});

	// 勾选主题
	$(".themeItem ul li").on('click', function() {
		$(".themeItem ul li").removeClass('themeActive');
		$(this).addClass('themeActive');
	});

	// 退出系统
	$("#logout").on('click', function() {
		$.messager.confirm('提示', '确定退出系统？', function(r) {
			if(r) {
				console.log('确定退出')
			}
		});
	});
});