/**
 * 关闭layer弹出框
 * 
 * @returns
 */
function closeDialog() {
	var index = parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
}

/**
 * 操作成功弹框
 * 
 * @returns
 */
function operSuccess() {
	layer.msg("操作成功", {
		icon : 1,
		time : 2000
	}, function() {
		closeDialog();
	});
}

/**
 * 操作失败弹框
 * 
 * @returns
 */
function operFail() {
	layer.msg("操作失败", {
		time : 2000,
		anim : 6
	});
}

/**
 * easyui ： 获取选择的行数据 
 * 规则 ： 有复选框选中时，取复选框选中的记录；反之取选中行记录
 * @param tableId
 *            表格的id
 * @returns 行记录集合
 */
function getSelRows(tableId) {
	var rows = $('#' + tableId).datagrid('getChecked');
	if (rows.length == 0) {
		rows = $('#' + tableId).datagrid('getSelections');
	}
	return rows;
}

/**
 * easyui 表格date类型转换显示
 * @param value
 * @param row
 * @param index
 * @returns
 */
function dateFormat(value, row, index) {
	if (!value) {
		return '';
	}
	var unixTimestamp = new Date(value);
	return unixTimestamp.toLocaleString();
}

/**
 * 封装查询参数
 * @param paramsAndValues 参数和参数值
 * @returns
 */
function conveterParamsToJson(paramsAndValues) {  
    var jsonObj = {};  
    var param = paramsAndValues.split("&");  
    for ( var i = 0; param != null && i < param.length; i++) {  
        var para = param[i].split("=");  
        jsonObj[para[0]] = para[1];  
    }  
  
    return jsonObj;  
}  

/**
 * 根据form的id去查询查询参数
 * @param form form的id
 * @returns
 */
function queryParamByFormId(form) { 
	// decodeURIComponent(data,true) 解决serialize()乱码问题
    var formValues = decodeURIComponent($("#" + form).serialize(),true); 
    console.log(formValues);
    //关于jquery的serialize方法转换空格为+号的解决方法  
    formValues = formValues.replace(/\+/g," ");
    var temp = JSON.stringify(conveterParamsToJson(formValues));  
    var queryParam = JSON.parse(temp);  
    return queryParam;  
}  


/**
 * 为什么easyui treegrid 的 reload 不了最后一条数据？？
 * @returns
 */
function refreshTab(){
	var currTab = $('#tt').tabs('getSelected');    
    var url = $(currTab.panel('options')).attr('href');                
    var title = $(currTab.panel('options')).attr('title');  
    var refeshTab = $('#tt').tabs("getTab", title);
    if (currTab != refeshTab) {
        tabPanel.tabs("select", title);
        currTab = refeshTab;
      }
    /* 重新设置该标签 */
    $('#tt').tabs('update', {
        tab: currTab,
        options: {
            // content: '<iframe src="' + url + '"  style="height: 100%; width: 100%;" ></iframe>'
        	 href: url
        }
    })
    currTab.panel('refresh');
}
