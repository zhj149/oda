package com.elvish.oda;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Criteria;
import org.nutz.dao.sql.Sql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.elvish.oda.busin.pojo.Role;
import com.elvish.oda.busin.pojo.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OdaApplicationTests {

	@Autowired
	Dao dao;

	@Test
	public void contextLoads() {

		List<User> users = new ArrayList<User>();
		for (int i = 10001; i < 100000; i++) {
			User user = new User();
			user.setCreateTime(new Date());
			user.setName("admin" + i);
			user.setDelflag(0);
			user.setUsername("admin" + i);
			user.setPassword("c4ca4238a0b923820dcc509a6f75849b");
			users.add(user);
		}

		dao.insert(users);
	}

	@Test
	public void insertUserRole() {
		String insertSql = "insert into t_user_role values(@userid,@roleid)";
		Sql sql = Sqls.fetchString(insertSql);
		for (int i = 1; i < 1000000; i++) {
			sql.params().set("userid", i + 100).set("roleid", 1);
			sql.addBatch();
		}
		dao.execute(sql);

	}

	@Test
	public void testDeletex() {
		String[] ids = { "3", "4" };
		Criteria cri = Cnd.cri();
		cri.where().andIn("id", ids);
		dao.clear(Role.class, cri);
	}

}
