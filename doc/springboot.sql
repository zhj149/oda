/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50622
Source Host           : localhost:3306
Source Database       : springboot

Target Server Type    : MYSQL
Target Server Version : 50622
File Encoding         : 65001

Date: 2018-01-16 16:58:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_area
-- ----------------------------
DROP TABLE IF EXISTS `t_area`;
CREATE TABLE `t_area` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` int(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `pid` int(20) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_area
-- ----------------------------
INSERT INTO `t_area` VALUES ('52', '江苏省', '1', '省份', null, '');
INSERT INTO `t_area` VALUES ('53', '南京市', '101', '市', '52', '');
INSERT INTO `t_area` VALUES ('54', '雨花台区', '10101', '区', '53', '');
INSERT INTO `t_area` VALUES ('55', '安徽省', '2', '省份', null, '');
INSERT INTO `t_area` VALUES ('56', '马鞍山市', '202', '市', '55', '');

-- ----------------------------
-- Table structure for t_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dept
-- ----------------------------
INSERT INTO `t_dept` VALUES ('1', '业务部');
INSERT INTO `t_dept` VALUES ('2', '技术部');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `icon` varchar(255) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `permission_code` varchar(255) DEFAULT NULL COMMENT '权限编码（菜单编码）',
  `pid` int(11) DEFAULT NULL COMMENT '父菜单id',
  `url` varchar(255) DEFAULT NULL COMMENT '菜单url跳转链接',
  `sortnum` int(3) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', 'icon-cog', '系统管理', 'xtgl', null, null, null);
INSERT INTO `t_menu` VALUES ('2', 'icon-city', '区域管理', 'qygl', '1', '/sys/areaManage', '1');
INSERT INTO `t_menu` VALUES ('3', '1', '机构管理', 'jggl', '1', '/sys/organizationManage', '2');
INSERT INTO `t_menu` VALUES ('4', 'icon-save', '用户管理', 'yhgl', '1', '/sys/userManage', '3');
INSERT INTO `t_menu` VALUES ('5', 'icon-save', '角色管理', 'jsgl', '1', '/sys/roleManage', '4');
INSERT INTO `t_menu` VALUES ('6', 'icon-menu', '菜单管理', 'cdgl', '1', '/sys/menuManage', '5');
INSERT INTO `t_menu` VALUES ('8', '1', '权限管理', 'qxgl', '1', '/sys/permissionManage', '6');
INSERT INTO `t_menu` VALUES ('30', '11', '系统日志', 'xtrz', '1', '/sys/syslog', '7');

-- ----------------------------
-- Table structure for t_organization
-- ----------------------------
DROP TABLE IF EXISTS `t_organization`;
CREATE TABLE `t_organization` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '机构名称',
  `area_id` int(10) DEFAULT NULL COMMENT '所属地域',
  `code` varchar(255) DEFAULT NULL COMMENT '机构编码',
  `type` varchar(255) DEFAULT NULL COMMENT '机构类型',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_organization
-- ----------------------------
INSERT INTO `t_organization` VALUES ('6', 'hoperun', '54', '123', '123', '3');
INSERT INTO `t_organization` VALUES ('7', 'nari', '53', '123', '123', '13');
INSERT INTO `t_organization` VALUES ('8', '小马科技', '54', '123', '123', '');

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `permission_code` varchar(255) DEFAULT NULL COMMENT '权限编码',
  `permission_name` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `type` int(2) DEFAULT NULL COMMENT '权限类型',
  `pid` int(11) DEFAULT NULL COMMENT '父类权限ID（依赖权限）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('20', 'xtgl', '系统管理', '1', null);
INSERT INTO `t_permission` VALUES ('21', 'qygl', '区域管理', '1', '20');
INSERT INTO `t_permission` VALUES ('23', 'jggl', '机构管理', '1', '20');
INSERT INTO `t_permission` VALUES ('24', 'yhgl', '用户管理', '1', '20');
INSERT INTO `t_permission` VALUES ('25', 'jsgl', '角色管理', '1', '20');
INSERT INTO `t_permission` VALUES ('26', 'cdgl', '菜单管理', '1', '20');
INSERT INTO `t_permission` VALUES ('27', 'qxgl', '权限管理', '1', '20');
INSERT INTO `t_permission` VALUES ('28', 'xtrz', '系统日志', '1', '20');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', 'VIP', '人民币玩家');
INSERT INTO `t_role` VALUES ('2', '平民', '穷B啊');

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `permission_id` int(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `FKjobmrl6dorhlfite4u34hciik` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------
INSERT INTO `t_role_permission` VALUES ('1', '4');
INSERT INTO `t_role_permission` VALUES ('1', '5');
INSERT INTO `t_role_permission` VALUES ('2', '20');
INSERT INTO `t_role_permission` VALUES ('2', '21');
INSERT INTO `t_role_permission` VALUES ('2', '23');
INSERT INTO `t_role_permission` VALUES ('2', '24');
INSERT INTO `t_role_permission` VALUES ('2', '25');
INSERT INTO `t_role_permission` VALUES ('2', '26');
INSERT INTO `t_role_permission` VALUES ('2', '27');
INSERT INTO `t_role_permission` VALUES ('2', '28');

-- ----------------------------
-- Table structure for t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log`;
CREATE TABLE `t_sys_log` (
  `id` bigint(200) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名称',
  `date` datetime DEFAULT NULL COMMENT '操作时间',
  `url` varchar(255) DEFAULT NULL COMMENT '请求url',
  `method` varchar(255) DEFAULT NULL COMMENT '请求方法',
  `ip` varchar(20) DEFAULT NULL COMMENT '请求ip',
  `class_method` varchar(255) DEFAULT NULL COMMENT '调用方法名称',
  `args` varchar(255) DEFAULT NULL COMMENT '请求参数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2259 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` datetime DEFAULT NULL COMMENT '创建人',
  `delflag` int(11) DEFAULT '0',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `name` varchar(255) DEFAULT NULL COMMENT '用户名称',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user` datetime DEFAULT NULL COMMENT '更新人员',
  `username` varchar(255) DEFAULT NULL COMMENT '登录名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', null, null, '0', '2018-01-16 16:38:33', '管理员', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin');
INSERT INTO `t_user` VALUES ('101', '2017-11-16 14:23:27', null, '0', '2017-12-06 14:43:25', 'admin1', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin1');
INSERT INTO `t_user` VALUES ('102', '2017-11-16 14:23:27', null, '0', null, 'admin2', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin2');
INSERT INTO `t_user` VALUES ('103', '2017-11-16 14:23:27', null, '0', null, 'admin3', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin3');
INSERT INTO `t_user` VALUES ('104', '2017-11-16 14:23:27', null, '0', null, 'admin4', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin4');
INSERT INTO `t_user` VALUES ('105', '2017-11-16 14:23:27', null, '0', null, 'admin5', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin5');
INSERT INTO `t_user` VALUES ('106', '2017-11-16 14:23:27', null, '0', null, 'admin6', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin6');
INSERT INTO `t_user` VALUES ('107', '2017-11-16 14:23:27', null, '0', null, 'admin7', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin7');
INSERT INTO `t_user` VALUES ('108', '2017-11-16 14:23:27', null, '0', null, 'admin8', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin8');
INSERT INTO `t_user` VALUES ('109', '2017-11-16 14:23:27', null, '0', null, 'admin9', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin9');
INSERT INTO `t_user` VALUES ('110', '2017-11-16 14:23:27', null, '0', null, 'admin10', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin10');
INSERT INTO `t_user` VALUES ('111', '2017-11-16 14:23:27', null, '0', null, 'admin11', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin11');
INSERT INTO `t_user` VALUES ('112', '2017-11-16 14:23:27', null, '0', null, 'admin12', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin12');
INSERT INTO `t_user` VALUES ('113', '2017-11-16 14:23:27', null, '0', null, 'admin13', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin13');
INSERT INTO `t_user` VALUES ('114', '2017-11-16 14:23:27', null, '0', null, 'admin14', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin14');
INSERT INTO `t_user` VALUES ('115', '2017-11-16 14:23:27', null, '0', null, 'admin15', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin15');
INSERT INTO `t_user` VALUES ('116', '2017-11-16 14:23:27', null, '0', null, 'admin16', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin16');
INSERT INTO `t_user` VALUES ('117', '2017-11-16 14:23:27', null, '0', null, 'admin17', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin17');
INSERT INTO `t_user` VALUES ('118', '2017-11-16 14:23:27', null, '0', null, 'admin18', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin18');
INSERT INTO `t_user` VALUES ('119', '2017-11-16 14:23:27', null, '0', null, 'admin19', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin19');
INSERT INTO `t_user` VALUES ('120', '2017-11-16 14:23:27', null, '0', null, 'admin20', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin20');
INSERT INTO `t_user` VALUES ('121', '2017-11-16 14:23:27', null, '0', null, 'admin21', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin21');
INSERT INTO `t_user` VALUES ('122', '2017-11-16 14:23:27', null, '0', null, 'admin22', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin22');
INSERT INTO `t_user` VALUES ('123', '2017-11-16 14:23:27', null, '0', null, 'admin23', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin23');
INSERT INTO `t_user` VALUES ('124', '2017-11-16 14:23:27', null, '0', null, 'admin24', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin24');
INSERT INTO `t_user` VALUES ('125', '2017-11-16 14:23:27', null, '0', null, 'admin25', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin25');
INSERT INTO `t_user` VALUES ('126', '2017-11-16 14:23:27', null, '0', null, 'admin26', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin26');
INSERT INTO `t_user` VALUES ('127', '2017-11-16 14:23:27', null, '0', null, 'admin27', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin27');
INSERT INTO `t_user` VALUES ('128', '2017-11-16 14:23:27', null, '0', null, 'admin28', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin28');
INSERT INTO `t_user` VALUES ('129', '2017-11-16 14:23:27', null, '0', null, 'admin29', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin29');
INSERT INTO `t_user` VALUES ('130', '2017-11-16 14:23:27', null, '0', null, 'admin30', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin30');
INSERT INTO `t_user` VALUES ('131', '2017-11-16 14:23:27', null, '0', null, 'admin31', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin31');
INSERT INTO `t_user` VALUES ('132', '2017-11-16 14:23:27', null, '0', null, 'admin32', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin32');
INSERT INTO `t_user` VALUES ('133', '2017-11-16 14:23:27', null, '0', null, 'admin33', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin33');
INSERT INTO `t_user` VALUES ('134', '2017-11-16 14:23:27', null, '0', null, 'admin34', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin34');
INSERT INTO `t_user` VALUES ('135', '2017-11-16 14:23:27', null, '0', null, 'admin35', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin35');
INSERT INTO `t_user` VALUES ('136', '2017-11-16 14:23:27', null, '0', null, 'admin36', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin36');
INSERT INTO `t_user` VALUES ('137', '2017-11-16 14:23:27', null, '0', null, 'admin37', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin37');
INSERT INTO `t_user` VALUES ('138', '2017-11-16 14:23:27', null, '0', null, 'admin38', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin38');
INSERT INTO `t_user` VALUES ('139', '2017-11-16 14:23:27', null, '0', null, 'admin39', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin39');
INSERT INTO `t_user` VALUES ('140', '2017-11-16 14:23:27', null, '0', null, 'admin40', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin40');
INSERT INTO `t_user` VALUES ('141', '2017-11-16 14:23:27', null, '1', null, 'admin41', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin41');
INSERT INTO `t_user` VALUES ('142', '2017-11-16 14:23:27', null, '0', null, 'admin42', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin42');
INSERT INTO `t_user` VALUES ('143', '2017-11-16 14:23:27', null, '0', null, 'admin43', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin43');
INSERT INTO `t_user` VALUES ('144', '2017-11-16 14:23:27', null, '0', null, 'admin44', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin44');
INSERT INTO `t_user` VALUES ('145', '2017-11-16 14:23:27', null, '0', null, 'admin45', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin45');
INSERT INTO `t_user` VALUES ('146', '2017-11-16 14:23:27', null, '0', null, 'admin46', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin46');
INSERT INTO `t_user` VALUES ('147', '2017-11-16 14:23:27', null, '0', null, 'admin47', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin47');
INSERT INTO `t_user` VALUES ('148', '2017-11-16 14:23:27', null, '0', null, 'admin48', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin48');
INSERT INTO `t_user` VALUES ('149', '2017-11-16 14:23:27', null, '0', null, 'admin49', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin49');
INSERT INTO `t_user` VALUES ('150', '2017-11-16 14:23:27', null, '0', null, 'admin50', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin50');
INSERT INTO `t_user` VALUES ('151', '2017-11-16 14:23:27', null, '0', null, 'admin51', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin51');
INSERT INTO `t_user` VALUES ('152', '2017-11-16 14:23:27', null, '0', null, 'admin52', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin52');
INSERT INTO `t_user` VALUES ('153', '2017-11-16 14:23:27', null, '0', null, 'admin53', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin53');
INSERT INTO `t_user` VALUES ('154', '2017-11-16 14:23:27', null, '0', null, 'admin54', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin54');
INSERT INTO `t_user` VALUES ('155', '2017-11-16 14:23:27', null, '0', null, 'admin55', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin55');
INSERT INTO `t_user` VALUES ('156', '2017-11-16 14:23:27', null, '0', null, 'admin56', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin56');
INSERT INTO `t_user` VALUES ('157', '2017-11-16 14:23:27', null, '0', null, 'admin57', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin57');
INSERT INTO `t_user` VALUES ('158', '2017-11-16 14:23:27', null, '0', null, 'admin58', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin58');
INSERT INTO `t_user` VALUES ('159', '2017-11-16 14:23:27', null, '0', null, 'admin59', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin59');
INSERT INTO `t_user` VALUES ('160', '2017-11-16 14:23:27', null, '0', null, 'admin60', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin60');
INSERT INTO `t_user` VALUES ('161', '2017-11-16 14:23:27', null, '0', null, 'admin61', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin61');
INSERT INTO `t_user` VALUES ('162', '2017-11-16 14:23:27', null, '0', null, 'admin62', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin62');
INSERT INTO `t_user` VALUES ('163', '2017-11-16 14:23:27', null, '0', null, 'admin63', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin63');
INSERT INTO `t_user` VALUES ('164', '2017-11-16 14:23:27', null, '0', null, 'admin64', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin64');
INSERT INTO `t_user` VALUES ('165', '2017-11-16 14:23:27', null, '0', null, 'admin65', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin65');
INSERT INTO `t_user` VALUES ('166', '2017-11-16 14:23:27', null, '0', null, 'admin66', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin66');
INSERT INTO `t_user` VALUES ('167', '2017-11-16 14:23:27', null, '0', null, 'admin67', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin67');
INSERT INTO `t_user` VALUES ('168', '2017-11-16 14:23:27', null, '0', null, 'admin68', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin68');
INSERT INTO `t_user` VALUES ('169', '2017-11-16 14:23:27', null, '0', null, 'admin69', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin69');
INSERT INTO `t_user` VALUES ('170', '2017-11-16 14:23:27', null, '0', null, 'admin70', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin70');
INSERT INTO `t_user` VALUES ('171', '2017-11-16 14:23:27', null, '0', null, 'admin71', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin71');
INSERT INTO `t_user` VALUES ('172', '2017-11-16 14:23:27', null, '0', null, 'admin72', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin72');
INSERT INTO `t_user` VALUES ('173', '2017-11-16 14:23:27', null, '0', null, 'admin73', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin73');
INSERT INTO `t_user` VALUES ('174', '2017-11-16 14:23:27', null, '0', null, 'admin74', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin74');
INSERT INTO `t_user` VALUES ('175', '2017-11-16 14:23:27', null, '0', null, 'admin75', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin75');
INSERT INTO `t_user` VALUES ('176', '2017-11-16 14:23:27', null, '0', null, 'admin76', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin76');
INSERT INTO `t_user` VALUES ('177', '2017-11-16 14:23:27', null, '0', null, 'admin77', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin77');
INSERT INTO `t_user` VALUES ('178', '2017-11-16 14:23:27', null, '0', null, 'admin78', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin78');
INSERT INTO `t_user` VALUES ('179', '2017-11-16 14:23:27', null, '0', null, 'admin79', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin79');
INSERT INTO `t_user` VALUES ('180', '2017-11-16 14:23:27', null, '0', null, 'admin80', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin80');
INSERT INTO `t_user` VALUES ('181', '2017-11-16 14:23:27', null, '0', null, 'admin81', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin81');
INSERT INTO `t_user` VALUES ('182', '2017-11-16 14:23:27', null, '0', null, 'admin82', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin82');
INSERT INTO `t_user` VALUES ('183', '2017-11-16 14:23:27', null, '0', null, 'admin83', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin83');
INSERT INTO `t_user` VALUES ('184', '2017-11-16 14:23:27', null, '0', null, 'admin84', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin84');
INSERT INTO `t_user` VALUES ('185', '2017-11-16 14:23:27', null, '0', null, 'admin85', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin85');
INSERT INTO `t_user` VALUES ('186', '2017-11-16 14:23:27', null, '0', null, 'admin86', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin86');
INSERT INTO `t_user` VALUES ('187', '2017-11-16 14:23:27', null, '0', null, 'admin87', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin87');
INSERT INTO `t_user` VALUES ('188', '2017-11-16 14:23:27', null, '0', null, 'admin88', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin88');
INSERT INTO `t_user` VALUES ('189', '2017-11-16 14:23:27', null, '0', null, 'admin89', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin89');
INSERT INTO `t_user` VALUES ('190', '2017-11-16 14:23:27', null, '0', null, 'admin90', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin90');
INSERT INTO `t_user` VALUES ('191', '2017-11-16 14:23:27', null, '0', null, 'admin91', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin91');
INSERT INTO `t_user` VALUES ('192', '2017-11-16 14:23:27', null, '0', null, 'admin92', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin92');
INSERT INTO `t_user` VALUES ('193', '2017-11-16 14:23:27', null, '0', null, 'admin93', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin93');
INSERT INTO `t_user` VALUES ('194', '2017-11-16 14:23:27', null, '0', null, 'admin94', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin94');
INSERT INTO `t_user` VALUES ('195', '2017-11-16 14:23:27', null, '0', null, 'admin95', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin95');
INSERT INTO `t_user` VALUES ('196', '2017-11-16 14:23:27', null, '0', null, 'admin96', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin96');
INSERT INTO `t_user` VALUES ('197', '2017-11-16 14:23:27', null, '0', null, 'admin97', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin97');
INSERT INTO `t_user` VALUES ('198', '2017-11-16 14:23:27', null, '0', null, 'admin98', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin98');
INSERT INTO `t_user` VALUES ('199', '2017-11-16 14:23:27', null, '0', null, 'admin99', 'c4ca4238a0b923820dcc509a6f75849b', null, null, null, null, 'admin99');

-- ----------------------------
-- Table structure for t_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `t_user_dept`;
CREATE TABLE `t_user_dept` (
  `userid` int(10) NOT NULL COMMENT '用户ID',
  `deptid` int(10) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`userid`,`deptid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_dept
-- ----------------------------
INSERT INTO `t_user_dept` VALUES ('1', '1');
INSERT INTO `t_user_dept` VALUES ('101', '2');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKa9c8iiy6ut0gnx491fqx4pxam` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('101', '1');
INSERT INTO `t_user_role` VALUES ('103', '1');
INSERT INTO `t_user_role` VALUES ('104', '1');
INSERT INTO `t_user_role` VALUES ('105', '1');
INSERT INTO `t_user_role` VALUES ('106', '1');
INSERT INTO `t_user_role` VALUES ('107', '1');
INSERT INTO `t_user_role` VALUES ('108', '1');
INSERT INTO `t_user_role` VALUES ('109', '1');
INSERT INTO `t_user_role` VALUES ('110', '1');
INSERT INTO `t_user_role` VALUES ('111', '1');
INSERT INTO `t_user_role` VALUES ('112', '1');
INSERT INTO `t_user_role` VALUES ('113', '1');
INSERT INTO `t_user_role` VALUES ('114', '1');
INSERT INTO `t_user_role` VALUES ('115', '1');
INSERT INTO `t_user_role` VALUES ('116', '1');
INSERT INTO `t_user_role` VALUES ('117', '1');
INSERT INTO `t_user_role` VALUES ('118', '1');
INSERT INTO `t_user_role` VALUES ('119', '1');
INSERT INTO `t_user_role` VALUES ('120', '1');
INSERT INTO `t_user_role` VALUES ('121', '1');
INSERT INTO `t_user_role` VALUES ('122', '1');
INSERT INTO `t_user_role` VALUES ('123', '1');
INSERT INTO `t_user_role` VALUES ('124', '1');
INSERT INTO `t_user_role` VALUES ('125', '1');
INSERT INTO `t_user_role` VALUES ('126', '1');
INSERT INTO `t_user_role` VALUES ('127', '1');
INSERT INTO `t_user_role` VALUES ('128', '1');
INSERT INTO `t_user_role` VALUES ('129', '1');
INSERT INTO `t_user_role` VALUES ('130', '1');
INSERT INTO `t_user_role` VALUES ('131', '1');
INSERT INTO `t_user_role` VALUES ('132', '1');
INSERT INTO `t_user_role` VALUES ('133', '1');
INSERT INTO `t_user_role` VALUES ('134', '1');
INSERT INTO `t_user_role` VALUES ('135', '1');
INSERT INTO `t_user_role` VALUES ('136', '1');
INSERT INTO `t_user_role` VALUES ('137', '1');
INSERT INTO `t_user_role` VALUES ('138', '1');
INSERT INTO `t_user_role` VALUES ('139', '1');
INSERT INTO `t_user_role` VALUES ('140', '1');
INSERT INTO `t_user_role` VALUES ('141', '1');
INSERT INTO `t_user_role` VALUES ('142', '1');
INSERT INTO `t_user_role` VALUES ('143', '1');
INSERT INTO `t_user_role` VALUES ('144', '1');
INSERT INTO `t_user_role` VALUES ('145', '1');
INSERT INTO `t_user_role` VALUES ('146', '1');
INSERT INTO `t_user_role` VALUES ('147', '1');
INSERT INTO `t_user_role` VALUES ('148', '1');
INSERT INTO `t_user_role` VALUES ('149', '1');
INSERT INTO `t_user_role` VALUES ('150', '1');
INSERT INTO `t_user_role` VALUES ('151', '1');
INSERT INTO `t_user_role` VALUES ('152', '1');
INSERT INTO `t_user_role` VALUES ('153', '1');
INSERT INTO `t_user_role` VALUES ('154', '1');
INSERT INTO `t_user_role` VALUES ('155', '1');
INSERT INTO `t_user_role` VALUES ('156', '1');
INSERT INTO `t_user_role` VALUES ('157', '1');
INSERT INTO `t_user_role` VALUES ('158', '1');
INSERT INTO `t_user_role` VALUES ('159', '1');
INSERT INTO `t_user_role` VALUES ('160', '1');
INSERT INTO `t_user_role` VALUES ('161', '1');
INSERT INTO `t_user_role` VALUES ('162', '1');
INSERT INTO `t_user_role` VALUES ('163', '1');
INSERT INTO `t_user_role` VALUES ('164', '1');
INSERT INTO `t_user_role` VALUES ('165', '1');
INSERT INTO `t_user_role` VALUES ('166', '1');
INSERT INTO `t_user_role` VALUES ('167', '1');
INSERT INTO `t_user_role` VALUES ('168', '1');
INSERT INTO `t_user_role` VALUES ('169', '1');
INSERT INTO `t_user_role` VALUES ('170', '1');
INSERT INTO `t_user_role` VALUES ('171', '1');
INSERT INTO `t_user_role` VALUES ('172', '1');
INSERT INTO `t_user_role` VALUES ('173', '1');
INSERT INTO `t_user_role` VALUES ('174', '1');
INSERT INTO `t_user_role` VALUES ('175', '1');
INSERT INTO `t_user_role` VALUES ('176', '1');
INSERT INTO `t_user_role` VALUES ('177', '1');
INSERT INTO `t_user_role` VALUES ('178', '1');
INSERT INTO `t_user_role` VALUES ('179', '1');
INSERT INTO `t_user_role` VALUES ('180', '1');
INSERT INTO `t_user_role` VALUES ('181', '1');
INSERT INTO `t_user_role` VALUES ('182', '1');
INSERT INTO `t_user_role` VALUES ('183', '1');
INSERT INTO `t_user_role` VALUES ('184', '1');
INSERT INTO `t_user_role` VALUES ('185', '1');
INSERT INTO `t_user_role` VALUES ('186', '1');
INSERT INTO `t_user_role` VALUES ('187', '1');
INSERT INTO `t_user_role` VALUES ('188', '1');
INSERT INTO `t_user_role` VALUES ('189', '1');
INSERT INTO `t_user_role` VALUES ('190', '1');
INSERT INTO `t_user_role` VALUES ('191', '1');
INSERT INTO `t_user_role` VALUES ('192', '1');
INSERT INTO `t_user_role` VALUES ('193', '1');
INSERT INTO `t_user_role` VALUES ('194', '1');
INSERT INTO `t_user_role` VALUES ('195', '1');
INSERT INTO `t_user_role` VALUES ('196', '1');
INSERT INTO `t_user_role` VALUES ('197', '1');
INSERT INTO `t_user_role` VALUES ('198', '1');
INSERT INTO `t_user_role` VALUES ('199', '1');
INSERT INTO `t_user_role` VALUES ('1', '2');
INSERT INTO `t_user_role` VALUES ('102', '2');
